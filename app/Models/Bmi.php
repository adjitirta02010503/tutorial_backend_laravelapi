<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bmi extends Model
{
    use HasFactory;
    // use SoftDeletes; // Use SoftDeletes library
    public $timestamps = true;
    protected $fillable = [
        'name',
        'umur',
        'alamat',
        'jenis_kelamin',
        'hobby',
        'tinggi',
        'berat'
    ];
    protected $table = 'bmi';

    public function drop(int $id)
    {
        return $this->find($id)->delete();
    }

    public function edit(array $payload, int $id)
    {
        return $this->find($id)->update($payload);
    }

    public function getAll(array $filter)
    {
        $app = $this->query();

        return $app->get();
    }

    public function getById(int $id)
    {
        return $this->find($id);
    }

    public function store(array $payload)
    {
        return $this->create($payload);
    }
}
