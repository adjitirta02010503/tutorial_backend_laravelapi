<?php

namespace App\Http\Controllers;

use App\Http\Resources\Bmi\BmiResource;
use App\Http\Resources\Bmi\BmiCollection;
use App\Models\Bmi;
use Illuminate\Http\Request;

class BmiController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    private $bmi;

    public function __construct()
    {
        $this->bmi = new Bmi();
    }

    public function index(Request $request)
    {
        // FITUR Search
        $filter = [
        'name' => $request->name ?? '',
        ];
        $bmis = $this->bmi->getAll($filter);

        return response()->json([
            "data" => $bmis,
            "status" => "success",
            "message" => "Berhasil mendapatkan data"
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $payload = $request->only([
            'name',
            'umur',
            'alamat',
            'jenis_kelamin',
            'hobby',
            'tinggi',
            'berat'
        ]);
        $bmi = $this->bmi->create($payload);

        return response()->json([
            "data" => $bmi,
            "status" => "success",
            "message" => "Berhasil menambahkan data"
        ], 200);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request)
    {

        $payload = $request->only([
            'name',
            'umur',
            'alamat',
            'jenis_kelamin',
            'hobby',
            'tinggi',
            'berat',
            'id'
        ]);

        $bmi = $this->bmi->edit($payload, $payload['id'] ?? 0);

        return response()->json([
            "data" => $payload,
            "status" => "success",
            "message" => "Berhasil ubah data"
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            $this->bmi->drop($id);
            return response()->json([
                "data" => $id,
                "status" => "success",
                "message" => "Berhasil hapus data"
            ], 200);
        } catch (Throwable $th) {
            return false;
        }
    }
}
