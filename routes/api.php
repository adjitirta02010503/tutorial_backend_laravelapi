<?php

use Illuminate\Http\Request;
use App\Http\Controllers\BmiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('bmi')->group(function () {
    Route::get('/', [BmiController::class, 'index']);
    Route::post('/', [BmiController::class, 'store']);
    Route::put('/', [BmiController::class, 'update']);
    Route::delete('/{id}', [BmiController::class, 'destroy']);
});

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
